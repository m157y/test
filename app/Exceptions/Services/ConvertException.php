<?php

namespace App\Exceptions\Services;

use RuntimeException;

/**
 * Exception class for money conversion
 */
class ConvertException extends RuntimeException
{
}