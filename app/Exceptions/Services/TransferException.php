<?php

namespace App\Exceptions\Services;

use RuntimeException;

/**
 * Exception class for money transfer
 */
class TransferException extends RuntimeException
{
}