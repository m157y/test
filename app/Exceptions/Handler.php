<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as IlluminateHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Handler extends IlluminateHandler
{
    /**
     * {@inheritdoc}
     */
    protected $internalDontReport = [];

    /**
     * {@inheritdoc}
     */
    protected function prepareException(Exception $e)
    {
        if ($e instanceof ValidationException) {
            $e = new BadRequestHttpException($e->getMessage(), $e);
        } else {
            $e = parent::prepareException($e);
        }

        return $e;
    }
}