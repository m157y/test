<?php

namespace App\Repositories;

use App\Models\Currency;
use App\Models\User;
use Throwable;

class UserRepository extends BaseRepository
{
    /**
     * Create user
     *
     * @param string $name User name
     * @param string $country Country name
     * @param string $city City name
     * @param Currency $currency Currency
     * @throws Throwable If unable to create user
     * @return User
     */
    public function create(
        string $name,
        string $country,
        string $city,
        Currency $currency
    ): User {
        $user = $this->container->make(User::class);

        $user
            ->fill([
                'currencyId' => $currency->id,
                'name' => $name,
                'country' => $country,
                'city' => $city,
            ])
            ->saveOrFail();

        return $user;
    }

    /**
     * Get user by name
     *
     * @param string $name Name
     * @return User|null
     */
    public function getByName(string $name): ?User
    {
        try {
            return $this->getByNameOrFail($name);
        } catch (Throwable $e) {
            return null;
        }
    }

    /**
     * Get user by name or fail
     *
     * @param string $name Name
     * @throws Throwable If user not found
     * @return User
     */
    public function getByNameOrFail(string $name): User
    {
        return $this->container
            ->make(User::class)
            ->where(['name' => $name])
            ->firstOrFail();
    }
}