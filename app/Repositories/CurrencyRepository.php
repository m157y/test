<?php

namespace App\Repositories;

use App\Models\Currency;
use Throwable;

class CurrencyRepository extends BaseRepository
{
    /**
     * Create currency
     *
     * @param string $name
     * @throws Throwable If unable to create currency
     * @return Currency
     */
    public function create(string $name): Currency
    {
        $currency = $this->container->get(Currency::class);

        $currency
            ->fill(['name' => $name])
            ->saveOrFail();

        return $currency;
    }

    /**
     * Get currency by name
     *
     * @param string $name Name
     * @return Currency|null
     */
    public function getByName(string $name): ?Currency
    {
        try {
            return $this->getByNameOrFail($name);
        } catch (Throwable $e) {
            return null;
        }
    }

    /**
     * Get currency by name or create
     *
     * @param string $name Name
     * @throws Throwable If unable to create currency
     * @return Currency
     */
    public function getByNameOrCreate(string $name): Currency
    {
        try {
            return $this->getByNameOrFail($name);
        } catch (Throwable $e) {
            return $this->create($name);
        }
    }

    /**
     * Get currency by name or fail
     *
     * @param string $name Name
     * @throws Throwable If currency not found
     * @return Currency
     */
    public function getByNameOrFail(string $name): Currency
    {
        return $this->container
            ->make(Currency::class)
            ->where(['name' => $name])
            ->firstOrFail();
    }
}