<?php

namespace App\Repositories;

use Illuminate\Contracts\Container\Container;

class BaseRepository
{
    /**
     * Container
     *
     * @var Container
     */
    protected $container;

    /**
     * @constructor
     * @param Container $container Container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
}
