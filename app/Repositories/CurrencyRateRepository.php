<?php

namespace App\Repositories;

use App\Models\Currency;
use App\Models\CurrencyRate;
use DateTime;
use Throwable;

class CurrencyRateRepository extends BaseRepository
{
    /**
     * Save currency rate
     *
     * @param Currency $currency Currency
     * @param string $date Date
     * @param float $rate Rate
     * @throws Throwable If unable to save currency rate
     * @return CurrencyRate
     */
    public function save(
        Currency $currency,
        string $date,
        float $rate
    ): CurrencyRate {
        $currencyRate = $this->container
            ->make(CurrencyRate::class)
            ->firstOrNew([
                'currencyId' => $currency->id,
                'date' => $date,
            ]);

        $currencyRate->rate = $rate;
        $currencyRate->saveOrFail();

        return $currencyRate;
    }

    /**
     * Get latest by currency or create empty instance if there is no rates
     *
     * @param Currency $currency Currency
     * @return CurrencyRate
     */
    public function getLatestByCurrency(Currency $currency): CurrencyRate
    {
        $currencyRate = $this->container
            ->make(CurrencyRate::class)
            ->newQuery()
            ->where('currencyId', '=', $currency->id)
            ->orderByDesc('date')
            ->first();

        if (!$currencyRate) {
            $currencyRate = $this->container
                ->make(CurrencyRate::class)
                ->fill([
                    'currencyId' => $currency->id,
                    'rate' => 1.0,
                    'date' => (new DateTime())->format('d-m-Y'),
                ]);
        }

        return $currencyRate;
    }
}