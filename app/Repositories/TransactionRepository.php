<?php

namespace App\Repositories;

use App\Models\Transaction;
use App\Models\User;
use App\Services\CurrencyRateService;
use Throwable;

class TransactionRepository extends BaseRepository
{
    /**
     * Create transaction
     *
     * @param string $type Type
     * @param User $user User
     * @param User|null $counterpart Counterpart
     * @param float $amount Amount
     * @throws Throwable If unable to create transaction
     * @return Transaction
     */
    public function create(
        string $type,
        User $user,
        ?User $counterpart,
        float $amount
    ): Transaction {
        $transaction = $this->container->make(Transaction::class);

        $transaction
            ->fill([
                'userId' => $user->id,
                'counterpartId' => $counterpart->id ?? null,
                'currencyId' => $user->currencyId,
                'type' => $type,
                'amount' => $amount,
                'amountUsd' => $this->container
                    ->make(CurrencyRateService::class)
                    ->convertToUsd($amount, $user->currency),
            ])
            ->saveOrFail();

        return $transaction;
    }

    /**
     * Create top up transaction
     *
     * @param User $user User
     * @param float $amount Amount
     * @throws Throwable If unable to create transaction
     * @return Transaction
     */
    public function topUp(
        User $user,
        float $amount
    ): Transaction {
        return $this->create(
            Transaction::TYPE_TOP_UP,
            $user,
            null,
            $amount
        );
    }

    /**
     * Create transfer transaction
     *
     * @param User $user User
     * @param User $counterpart Counterpart
     * @param float $amount Amount
     * @throws Throwable If unable to create transaction
     * @return Transaction
     */
    public function transfer(
        User $user,
        User $counterpart,
        float $amount
    ): Transaction {
        return $this->create(
            Transaction::TYPE_TRANSFER,
            $user,
            $counterpart,
            $amount
        );
    }
}