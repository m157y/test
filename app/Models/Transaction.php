<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id ID
 * @property int $userId User ID
 * @property int|null $counterpartId Counterpart ID
 * @property int $currencyId Currency ID
 * @property string $type Type
 * @property float $amount Amount in selected currency
 * @property float $amountUsd Amount in USD
 * @property string $createdAt Timestamp of record creating
 * @property string $updatedAt Timestamp of record updating
 *
 * @property-read User $user User
 * @property-read User|null $counterpart Counterpart
 * @property-read Currency $currency Currency
 *
 * @mixin Eloquent
 */
class Transaction extends Model
{
    /**
     * {@inheritdoc}
     */
    public const CREATED_AT = 'createdAt';

    /**
     * {@inheritdoc}
     */
    public const UPDATED_AT = 'updatedAt';

    /**
     * Type top up
     *
     * @const string
     */
    public const TYPE_TOP_UP = 'topUp';

    /**
     * Type transfer
     *
     * @const string
     */
    public const TYPE_TRANSFER = 'transfer';

    /**
     * {@inheritdoc}
     */
    protected $table = 'transactions';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'userId',
        'counterpartId',
        'currencyId',
        'type',
        'amount',
        'amountUsd',
    ];

    /**
     * User relation
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(
            User::class,
            'userId',
            'id'
        );
    }

    /**
     * Counterpart relation
     *
     * @return BelongsTo
     */
    public function counterpart(): BelongsTo
    {
        return $this->belongsTo(
            User::class,
            'counterpartId',
            'id'
        );
    }

    /**
     * Currency relation
     *
     * @return HasOne
     */
    public function currency(): HasOne
    {
        return $this->hasOne(
            Currency::class,
            'id',
            'currencyId'
        );
    }
}
