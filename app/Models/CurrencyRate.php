<?php

namespace App\Models;

use DateTime;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id ID
 * @property int $currencyId Currency ID
 * @property float $rate Currency rate to USD
 * @property string $date Date of currency rate
 * @property string $createdAt Timestamp of record creating
 * @property string $updatedAt Timestamp of record updating
 *
 * @mixin Eloquent
 */
class CurrencyRate extends Model
{
    /**
     * {@inheritdoc}
     */
    public const CREATED_AT = 'createdAt';

    /**
     * {@inheritdoc}
     */
    public const UPDATED_AT = 'updatedAt';

    /**
     * {@inheritdoc}
     */
    protected $table = 'currencyRates';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'currencyId',
        'rate',
        'date',
    ];

    /**
     * Find latest rate by currency ID
     *
     * @param int $currencyId Currency ID
     * @return CurrencyRate
     */
    public static function latestByCurrencyId(int $currencyId): CurrencyRate
    {
        $currencyRate = (new static())
            ->newQuery()
            ->where('currencyId', '=', $currencyId)
            ->orderByDesc('date')
            ->first();

        if (!$currencyRate) {
            $currencyRate = new static([
                'currencyId' => $currencyId,
                'rate' => 1.0,
                'date' => (new DateTime())->format('d-m-Y'),
            ]);
        }

        return $currencyRate;
    }
}
