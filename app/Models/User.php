<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id ID
 * @property int $currencyId Currency ID
 * @property string $name Username
 * @property string $country Country name
 * @property string $city City name
 * @property float $balance Balance in selected currency
 * @property string $createdAt Timestamp of record creating
 * @property string $updatedAt Timestamp of record updating
 *
 * @property-read Currency $currency Currency
 *
 * @mixin Eloquent
 */
class User extends Model
{
    /**
     * {@inheritdoc}
     */
    public const CREATED_AT = 'createdAt';

    /**
     * {@inheritdoc}
     */
    public const UPDATED_AT = 'updatedAt';

    /**
     * {@inheritdoc}
     */
    protected $table = 'users';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'currencyId',
        'name',
        'country',
        'city',
    ];

    /**
     * Currency relation
     *
     * @return HasOne
     */
    public function currency(): HasOne
    {
        return $this->hasOne(
            Currency::class,
            'id',
            'currencyId'
        );
    }
}
