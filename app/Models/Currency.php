<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id ID
 * @property string $name Currency name
 * @property string $createdAt Timestamp of record creating
 * @property string $updatedAt Timestamp of record updating
 *
 * @mixin Eloquent
 */
class Currency extends Model
{
    /**
     * {@inheritdoc}
     */
    public const CREATED_AT = 'createdAt';

    /**
     * {@inheritdoc}
     */
    public const UPDATED_AT = 'updatedAt';

    /**
     * {@inheritdoc}
     */
    protected $table = 'currencies';

    /**
     * {@inheritdoc}
     */
    protected $fillable = ['name'];
}
