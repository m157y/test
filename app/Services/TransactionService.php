<?php

namespace App\Services;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Collection;

class TransactionService extends BaseService
{
    /**
     * Generate transactions report
     *
     * @param User $user User
     * @param null|string $from Period from
     * @param null|string $to Period to
     * @return Collection
     */
    public function report(
        User $user,
        ?string $from = null,
        ?string $to = null
    ): Collection {
        $transaction = $this->container->make(Transaction::class);

        $query = $transaction
            ->newQuery()
            ->where('userId', $user->id)
            ->orderByDesc('createdAt');

        if ($from) {
            $query->where('createdAt', '>=', $from);
        }

        if ($to) {
            $query->where('createdAt', '<=', $to);
        }

        return $query->get();
    }
}