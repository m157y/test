<?php

namespace App\Services;

use Illuminate\Contracts\Container\Container;

class BaseService
{
    /**
     * Container
     *
     * @var Container
     */
    protected $container;

    /**
     * @constructor
     * @param Container $container Container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
}
