<?php

namespace App\Services;

use App\Exceptions\Services\ConvertException;
use App\Models\Currency;
use App\Models\CurrencyRate;
use App\Models\User;
use App\Repositories\CurrencyRateRepository;

class CurrencyRateService extends BaseService
{
    /**
     * Convert currency rates
     *
     * @param float $amount Amount to convert
     * @param CurrencyRate $from Original currency rate
     * @param CurrencyRate $to Result currency rate
     * @return float
     */
    public function convert(
        float $amount,
        CurrencyRate $from,
        CurrencyRate $to
    ): float {
        return $amount
            * $from->rate
            / $to->rate;
    }

    /**
     * Convert currency rate to usd
     *
     * @param float $amount Amount
     * @param Currency $currency Currency
     * @return float
     */
    public function convertToUsd(float $amount, Currency $currency): float
    {
        $currencyRate = $this->container
            ->make(CurrencyRateRepository::class)
            ->getLatestByCurrency($currency);

        return $amount * $currencyRate->rate;
    }

    /**
     * Convert transferable amount for sender and recipient
     *
     * @param User $sender Sender
     * @param User $recipient Recipient
     * @param Currency $currency Currency
     * @param float $amount Amount
     * @throws ConvertException If malformed currency was provided
     * @return array
     */
    public function convertForTransfer(
        User $sender,
        User $recipient,
        Currency $currency,
        float $amount
    ): array {
        $currencyRateRepository = $this->container->make(CurrencyRateRepository::class);

        $senderCurrencyRate = $currencyRateRepository->getLatestByCurrency($sender->currency);
        $recipientCurrencyRate = $currencyRateRepository->getLatestByCurrency($recipient->currency);

        if ($sender->currencyId === $currency->id) {
            $senderAmount = $amount;
            $recipientAmount = $this->convert(
                $amount,
                $senderCurrencyRate,
                $recipientCurrencyRate
            );
        } elseif ($recipient->currencyId === $currency->id) {
            $senderAmount = $this->convert(
                $amount,
                $recipientCurrencyRate,
                $senderCurrencyRate
            );
            $recipientAmount = $amount;
        } else {
            throw new ConvertException('Currency of sender or recipient should be used');
        }

        return [
            'sender' => $senderAmount,
            'recipient' => $recipientAmount,
        ];
    }
}