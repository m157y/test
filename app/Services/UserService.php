<?php

namespace App\Services;

use App\Exceptions\Services\ConvertException;
use App\Exceptions\Services\TransferException;
use App\Models\Currency;
use App\Models\User;
use App\Repositories\TransactionRepository;
use Throwable;

class UserService extends BaseService
{
    /**
     * Top up user balance
     *
     * @param User $user User
     * @param float $amount Amount
     * @throws Throwable If unable to save to database
     * @return void
     */
    public function topUp(User $user, float $amount): void
    {
        $transactionRepository = $this->container->make(TransactionRepository::class);

        $this->container
            ->make('db.connection')
            ->transaction(static function () use (
                $transactionRepository,
                $user,
                $amount
            ): void {
                $user->increment('balance', $amount);

                $transactionRepository->topUp($user, $amount);
            });
    }

    /**
     * Transfer money from one user to another
     *
     * @param User $sender Sender
     * @param User $recipient Recipient
     * @param Currency $currency Currency
     * @param float $amount Amount
     * @throws ConvertException If malformed currency was selected
     * @throws TransferException If user does not have enough money
     * @throws Throwable If unable to save to database
     * @return void
     */
    public function transfer(
        User $sender,
        User $recipient,
        Currency $currency,
        float $amount
    ): void {
        $transactionRepository = $this->container->make(TransactionRepository::class);

        [
            'sender' => $senderAmount,
            'recipient' => $recipientAmount,
        ] = $this->container
            ->make(CurrencyRateService::class)
            ->convertForTransfer(
                $sender,
                $recipient,
                $currency,
                $amount
            );

        if ($sender->balance < $senderAmount) {
            throw new TransferException('Sender does not have enough money');
        }

        $this->container
            ->make('db.connection')
            ->transaction(static function () use (
                $transactionRepository,
                $sender,
                $recipient,
                $senderAmount,
                $recipientAmount
            ): void {
                $sender->decrement('balance', $senderAmount);
                $recipient->increment('balance', $recipientAmount);

                $transactionRepository->transfer(
                    $sender,
                    $recipient,
                    -$senderAmount
                );

                $transactionRepository->transfer(
                    $recipient,
                    $sender,
                    $recipientAmount
                );
            });
    }
}