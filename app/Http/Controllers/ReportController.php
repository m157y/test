<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Repositories\UserRepository;
use App\Services\TransactionService;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;
use Laracsv\Export;
use Throwable;

class ReportController extends BaseController
{
    use ValidatesRequests;

    /**
     * Validation rules for transactions report
     *
     * @const string[]
     */
    protected const RULES_REPORT = [
        'name' => 'string|required|exists:users',
        'from' => 'string|date',
        'to' => 'string|date',
    ];

    /**
     * Generate transactions report
     *
     * @param Request $request Request
     * @throws ValidationException If request is not valid
     * @throws Throwable If user not found
     * @return mixed
     */
    public function index(Request $request)
    {
        $this->validate($request, self::RULES_REPORT);

        $name = $request->get('name');
        $from = $request->get('from');
        $to = $request->get('to');
        $isDownload = $request->has('download');

        $userRepository = app(UserRepository::class);
        $transactionService = app(TransactionService::class);

        $user = $userRepository->getByNameOrFail($name);
        $transactions = $transactionService->report($user, $from, $to);

        $total = 0;
        $totalUsd = 0;
        $totalIncome = 0;
        $totalIncomeUsd = 0;
        $totalExpense = 0;
        $totalExpenseUsd = 0;

        /** @var Transaction $transaction */
        foreach ($transactions as $transaction) {
            $total += $transaction->amount;
            $totalUsd += $transaction->amountUsd;

            if ($transaction->amount > 0) {
                $totalIncome += $transaction->amount;
                $totalIncomeUsd += $transaction->amountUsd;
            } else {
                $totalExpense += $transaction->amount;
                $totalExpenseUsd += $transaction->amountUsd;
            }
        }

        return $isDownload
            ? app(Export::class)
                ->build($transactions, ['type', 'amount', 'amountUsd', 'createdAt'])
                ->download($user['name'] . '.csv')
            : view(
                'report',
                [
                    'downloadLink' => $request->fullUrl() . '&download',
                    'user' => $user,
                    'currency' => $user->currency,
                    'transactions' => $transactions,
                    'total' => $total,
                    'totalUsd' => $totalUsd,
                    'totalIncome' => $totalIncome,
                    'totalIncomeUsd' => $totalIncomeUsd,
                    'totalExpense' => $totalExpense,
                    'totalExpenseUsd' => $totalExpenseUsd,
                ]
            );
    }
}
