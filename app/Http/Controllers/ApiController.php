<?php

namespace App\Http\Controllers;

use App\Exceptions\Services\ConvertException;
use App\Exceptions\Services\TransferException;
use App\Repositories\CurrencyRateRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;
use Throwable;

class ApiController extends BaseController
{
    use ValidatesRequests;

    /**
     * Validation rules for user registration
     *
     * @const string[]
     */
    protected const RULES_REGISTRATION = [
        'name' => 'string|required|min:1|unique:users',
        'country' => 'string|required|min:1',
        'city' => 'string|required|min:1',
        'currency' => 'string|required|min:1',
        'balance' => 'numeric|min:0',
    ];

    /**
     * Validation rules for top up
     *
     * @const string[]
     */
    protected const RULES_TOP_UP = [
        'name' => 'string|required|exists:users',
        'amount' => 'numeric|required|min:0',
    ];

    /**
     * Validation rules for transfer
     *
     * @const string[]
     */
    protected const RULES_TRANSFER = [
        'sender' => 'string|required|exists:users,name',
        'recipient' => 'string|required|exists:users,name|different:sender',
        'amount' => 'numeric|required|min:0',
        'currency' => 'string|required|exists:currencies,name',
    ];

    /**
     * Validation rules for currency rate setter
     *
     * @const string[]
     */
    protected const RULES_CURRENCY_RATE = [
        'name' => 'string|required|exists:currencies',
        'rate' => 'numeric|required|min:0',
        'date' => 'string|required|date_format:Y-m-d',
    ];

    /**
     * Register user
     *
     * @param Request $request Request
     * @throws ValidationException If request is not valid
     * @throws Throwable If unable to save user
     * @return Response
     */
    public function register(Request $request): Response
    {
        $this->validate($request, self::RULES_REGISTRATION);

        $userName = $request->get('name');
        $country = $request->get('country');
        $city = $request->get('city');
        $currencyName = $request->get('currency');

        $userRepository = app(UserRepository::class);
        $currencyRepository = app(CurrencyRepository::class);

        $currency = $currencyRepository->getByNameOrCreate($currencyName);
        $user = $userRepository->create($userName, $country, $city, $currency);

        return response([
            'user' => $user,
            'currency' => $currency,
        ]);
    }

    /**
     * Top up user balance
     *
     * @param Request $request Request
     * @throws ValidationException If request is not valid
     * @throws Throwable If user not found
     * @return Response
     */
    public function topUp(Request $request): Response
    {
        $this->validate($request, self::RULES_TOP_UP);

        $name = $request->get('name');
        $amount = (float) $request->get('amount');

        $userRepository = app(UserRepository::class);
        $userService = app(UserService::class);

        $user = $userRepository->getByNameOrFail($name);

        $userService->topUp($user, $amount);

        return response([
            'user' => $user,
        ]);
    }

    /**
     * Transfer money from one user to another
     *
     * @param Request $request Request
     * @throws ValidationException If request is not valid
     * @throws ConvertException If malformed currency rate was provided
     * @throws TransferException If sender does not have enough money
     * @throws Throwable If unable to transfer
     * @return Response
     */
    public function transfer(Request $request): Response
    {
        $this->validate($request, self::RULES_TRANSFER);

        $senderName = $request->get('sender');
        $recipientName = $request->get('recipient');
        $amount = (float) $request->get('amount');
        $currencyName = $request->get('currency');

        $userRepository = app(UserRepository::class);
        $currencyRepository = app(CurrencyRepository::class);
        $userService = app(UserService::class);

        $sender = $userRepository->getByNameOrFail($senderName);
        $recipient = $userRepository->getByNameOrFail($recipientName);
        $currency = $currencyRepository->getByNameOrFail($currencyName);

        $userService->transfer($sender, $recipient, $currency, $amount);

        return response([
            'sender' => $sender,
            'recipient' => $recipient,
        ]);
    }

    /**
     * Set currency rate for date
     *
     * @param Request $request Request
     * @throws ValidationException If request is not valid
     * @throws Throwable If unable to save currency rate
     * @return Response
     */
    public function setRate(Request $request): Response
    {
        $this->validate($request, self::RULES_CURRENCY_RATE);

        $name = $request->get('name');
        $rate = $request->get('rate');
        $date = $request->get('date');

        $currencyRepository = app(CurrencyRepository::class);
        $currencyRateRepository = app(CurrencyRateRepository::class);

        $currency = $currencyRepository->getByNameOrFail($name);
        $currencyRate = $currencyRateRepository->save($currency, $date, $rate);

        return response([
            'currency' => $currency,
            'currencyRate' => $currencyRate,
        ]);
    }
}
