<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Report</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" />
  </head>
  <body>
    <div class="container">

      <h2>
        Report for "{{ $user['name'] }}"
        <a href="{{ $downloadLink }}" class="btn btn-sm btn-light float-right mt-2">Download</a>
      </h2>

      <table class="table table-striped table-bordered table-hover table-sm">
        <thead class="thead-light">
          <tr class="text-center">
            <th scope="col">#</th>
            <th scope="col">Type</th>
            <th scope="col">Amount (in {{ $currency['name'] }})</th>
            <th scope="col">Amount (in USD)</th>
            <th scope="col">Date/Time</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($transactions as $index => $transaction)
            <tr>
              <th
                scope="row"
                @if ($transaction['amount'] > 0)
                  class="text-center bg-success"
                @else
                  class="text-center bg-danger"
                @endif
              >
                {{ $index + 1 }}
              </th>
              <td>
                {{ \Illuminate\Support\Str::studly($transaction['type']) }}
              </td>
              <td class="text-right">
                {{ $transaction['amount'] }} {{ $currency['name'] }}
              </td>
              <td class="text-right">
                {{ $transaction['amountUsd'] }} USD
              </td>
              <td class="text-right">
                {{ $transaction['createdAt'] }}
              </td>
            </tr>
          @endforeach
        </tbody>
        <tfoot class="thead-light text-right">
          <tr>
            <th scope="col">&nbsp;</th>
            <th>Total income:</th>
            <th>{{ $totalIncome }} {{ $currency['name'] }}</th>
            <th>{{ $totalIncomeUsd }} USD</th>
            <th>&nbsp;</th>
          </tr>
          <tr>
            <th scope="col">&nbsp;</th>
            <th>Total expense:</th>
            <th>{{ $totalExpense }} {{ $currency['name'] }}</th>
            <th>{{ $totalExpenseUsd }} USD</th>
            <th>&nbsp;</th>
          </tr>
          <tr>
            <th scope="col">&nbsp;</th>
            <th>Total:</th>
            <th>{{ $total }} {{ $currency['name'] }}</th>
            <th>{{ $totalUsd }} USD</th>
            <th>&nbsp;</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </body>
</html>
