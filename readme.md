#

CREATE USER 'test'@'%' IDENTIFIED WITH mysql_native_password BY 'password';

GRANT ALL PRIVILEGES ON *.* TO 'test'@'%';

FLUSH PRIVILEGES;

CREATE DATABASE 'test';

#

/api/register?country=Germany&city=Berlin&currency=EUR&balance=0&name=test1

/api/register?country=Germany&city=Berlin&currency=CAD&balance=0&name=test2

/api/topUp?name=test1&amount=100

/api/topUp?name=test2&amount=150

/api/setRate?name=EUR&rate=1.14&date=2019-01-27

/api/setRate?name=CAD&rate=0.76&date=2019-01-27

/api/transfer?sender=test1&recipient=test2&amount=1&currency=EUR

/api/transfer?sender=test1&recipient=test2&amount=1&currency=CAD

/?name=test1

/?name=test1&from=2019-01-27

/?name=test1&to=2019-01-28

/?name=test1&download
